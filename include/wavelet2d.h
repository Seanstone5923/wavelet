#ifndef WAVELET2D_H
#define WAVELET2D_H
#include <vector>
#include <complex>
using namespace std;

// the dll s
//#if defined WAVE_
//#define  __declspec(dll)
//#else
//#define  __declspec(dllimport)
//#endif

// 1D Functions


 int dwt1(string, vector<double> &, vector<double> &, vector<double> &);

 int dyadic_zpad_1d(vector<double> &);

 double convol(vector<double> &, vector<double> &, vector<double> &);

 int filtcoef(string , vector<double> &, vector<double> &, vector<double> &,
                vector<double> &);

 int downsamp(vector<double> &, int , vector<double> &);

 int upsamp(vector<double> &, int, vector<double> &);

 int circshift(vector<double> &, int );

 int sign(int);

 int idwt1(string wname, vector<double> &, vector<double> &, vector<double> &);

 int vecsum(vector<double> &, vector<double> &, vector<double> &);



// 1D Symmetric Extension DWT Functions



 int dwt_sym(vector<double> &, int ,string , vector<double> &,vector<double> &,
                vector<int> &);

 int dwt1_sym(string , vector<double> &, vector<double> &, vector<double> &);

 int idwt_sym(vector<double> &,vector<double> &, string,vector<double> &, vector<int> &);

 int symm_ext(vector<double> &, int );

 int idwt1_sym(string, vector<double> &, vector<double> &, vector<double> &); // Not Tested

// 1D Stationary Wavelet Transform

 int swt(vector<double> &, int , string , vector<double> &, int &) ;

 int iswt(vector<double> &,int , string, vector<double> &);

 int per_ext(vector<double> &, int );




// 2D Functions

 int branch_lp_dn(string , vector<double> &, vector<double> &);

 int branch_hp_dn(string , vector<double> &, vector<double> &);

 int branch_lp_hp_up(string ,vector<double> &, vector<double> &, vector<double> &);

// int dwt_2d(vector<vector<double> > &, int , string , vector<vector<double> > &
  //              , vector<double> &) ;

// int idwt_2d(vector<vector<double> > &,vector<double> &, string ,vector<vector<double> > &);

 int dyadic_zpad_2d(vector<vector<double> > &,vector<vector<double> > &);

 int dwt_output_dim(vector<vector<double> >&, int &, int & );

 int zero_remove(vector<vector<double> > &,vector<vector<double> > &) ;

 int getcoeff2d(vector<vector<double> > &, vector<vector<double> > &,
                vector<vector<double> > &,vector<vector<double> > &,vector<double> &, int &);

 int idwt2(string ,vector<vector<double> > &, vector<vector<double> >  &,
                vector<vector<double> >  &, vector<vector<double> >  &, vector<vector<double> > &);

 int dwt2(string ,vector<vector<double> > &, vector<vector<double> >  &,
                vector<vector<double> >  &, vector<vector<double> > &, vector<vector<double> > &);

 int downsamp2(vector<vector<double> > &,vector<vector<double> > &, int, int);

 int upsamp2(vector<vector<double> > &,vector<vector<double> > &, int, int);

// 2D DWT (Symmetric Extension) Functions

 int dwt_2d_sym(vector<vector<double> > &, int , string , vector<double> &, vector<double> & ,
                vector<int> &);

 int dwt2_sym(string ,vector<vector<double> > &, vector<vector<double> >  &,
                vector<vector<double> >  &, vector<vector<double> >  &, vector<vector<double> > &);

 int idwt_2d_sym(vector<double>  &,vector<double> &, string ,vector<vector<double> > &,
                vector<int> &);

 int circshift2d(vector<vector<double> > &, int , int );

 int symm_ext2d(vector<vector<double> > &,vector<vector<double> > &, int );

 int dispDWT(vector<double> &,vector<vector<double> > &, vector<int> &, vector<int> &, int ) ;

 int dwt_output_dim_sym(vector<int> &,vector<int> &, int );

//2D Stationary Wavelet Transform

 int swt_2d(vector<vector<double> > &,int , string , vector<double> &);

 int per_ext2d(vector<vector<double> > &,vector<vector<double> > &, int );

// FFT functions


 double convfft(vector<double> &, vector<double> &, vector<double> &);

 double convfftm(vector<double> &, vector<double> &, vector<double> &);

 int fft(vector<complex<double> > &,int ,unsigned int);

 int bitreverse(vector<complex<double> > &);

 int freq(vector<double> &, vector<double> &);

//New


 int dwt1_sym_m(string wname, vector<double> &signal, vector<double> &cA, vector<double> &cD);//FFTW3 for 2D

 int idwt1_sym_m(string wname, vector<double> &X, vector<double> &app, vector<double> &detail);

 int dwt(vector<double> &sig, int J, string nm, vector<double> &dwt_output
                , vector<double> &flag, vector<int> &length );

 int idwt(vector<double> &,vector<double> &, string,vector<double> &, vector<int> &);

 int dwt_2d(vector<vector<double> > &, int , string , vector<double> &, vector<double> & ,
                vector<int> &);
 int dwt1_m(string wname, vector<double> &signal, vector<double> &cA, vector<double> &cD) ;

 int idwt_2d(vector<double>  &dwtop,vector<double> &flag, string nm,
                vector<vector<double> > &idwt_output, vector<int> &length);

 int idwt1_m(string wname, vector<double> &X, vector<double> &cA, vector<double> &cD);

 int dwt_output_dim2(vector<int> &length, vector<int> &length2, int J);


#endif/* WAVELET2D_H */
